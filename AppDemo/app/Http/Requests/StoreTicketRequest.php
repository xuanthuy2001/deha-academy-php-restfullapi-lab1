<?php

namespace App\Http\Requests;

use Illuminate\Http\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;



class StoreTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price'=> 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ":attribute khong duoc de tronng",
            'price.required' => ":attribute khong duoc de trong",
            'price.integer' => ":attribute phai la chu"
        ];
    }

    public function  attributes()
    {
        return [
            "name"=>"Ten",
            "price"=>"Gia",
        ];
    }

    public function     failedValidation(Validator $validator)
    {
        $respone = new Response([
            'errors' =>$validator->errors(),
            'status_code'=>Response::HTTP_UNPROCESSABLE_ENTITY
        ],Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator,$respone));
    }

}
