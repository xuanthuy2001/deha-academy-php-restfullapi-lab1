<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\TicketCollection;
use App\Http\Resources\TicketResource;

class TicketController extends Controller
{
    protected $ticket;

    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    public function index()
    {
        $tickets = $this->ticket->paginate(5);
        $ticketCollection = new TicketCollection($tickets);
        return $this-> sendSuccessResponse($ticketCollection,'thanh cong',Response::HTTP_OK);
    }

    public function store(StoreTicketRequest $request)
    {
        $data = $request->all();
        $ticket  = $this->ticket->create($data);
        $ticketResource  = new TicketResource($ticket);

        return $this-> sendSuccessResponse($ticketResource,'them thanh cong',Response::HTTP_CREATED);
    }

    public function show($id)
    {
       $ticket = $this->ticket->findOrFail($id);
       $ticketResource = new TicketResource($ticket);

       return $this-> sendSuccessResponse($ticketResource,'thanh cong',Response::HTTP_OK);
    }

    public function update(UpdateTicketRequest $request, $id)
    {
        $ticket = $this->ticket->findOrFail($id);
        $dataUpdate = $request ->all();
        $ticket->update($dataUpdate);
        $ticketResource = new TicketResource($ticket);

        return $this-> sendSuccessResponse($ticketResource,'sua thanh cong',Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $ticket = $this->ticket->findOrFail($id);
        $ticket->delete();
        $ticketResource = new TicketResource($ticket);

        return $this-> sendSuccessResponse($ticketResource,'xoa thanh cong',Response::HTTP_OK);
    }
}
