<?php

namespace Tests\Feature\Tickets;

use App\Models\Ticket;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetShowTicketTest extends TestCase
{
   /** @test */
    public  function  user_can_get_ticket_if_ticket_exeits()
    {
        $ticket = Ticket::factory()->create();
        $response = $this->getJson(route('tickets.show',$ticket->id));
        $response ->assertStatus(Response::HTTP_OK);

        $response->assertJson(function (AssertableJson  $json){
            return $json->has('data')->has('message')->has('status_code');
        });
    }
    /** @test */
    public  function  user_can_not_get_ticket_if_ticket_not_exeits()
    {
       $ticketId= -1;

       $response = $this->getJson(route('tickets.show',$ticketId));
       $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(function (AssertableJson  $json){
            return $json->has('status')->etc();
        });
    }
}
