<?php

namespace Tests\Feature\Tickets;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetListTicketTest extends TestCase
{
    /** @test */

    public function user_can_get_list_tickets()
    {
        $response = $this->getJson(route('tickets.index'));
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has('data')->has('status_code')->etc();
        }
        );
    }
}
