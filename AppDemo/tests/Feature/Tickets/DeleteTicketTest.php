<?php

namespace Tests\Feature\Tickets;

use Tests\TestCase;
use App\Models\Ticket;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteTicketTest extends TestCase
{
    /** @test */

    public  function  user_can_delete_ticket()
    {
        $ticket = Ticket::factory()->create();

        $ticketCountBeforeDelete  = Ticket::count();
        $response = $this->Json('DELETE',route('tickets.destroy',$ticket->id));
        $response ->assertStatus(Response::HTTP_OK);

        $response->assertJson(function (AssertableJson  $json) use ($ticket) {
            return $json->has('data', function (AssertableJson $json) use ($ticket) {
                return $json->where('name', $ticket->name)->etc();
            })->etc();
        });

        $ticketCountAfterDelete  = Ticket::count();

        $this->assertEquals($ticketCountBeforeDelete-1,$ticketCountAfterDelete);
    }

    /** @test */
    public  function  user_can_not_delete_ticket_not_exits()
    {
        $ticketId= -1;

        $response = $this->getJson(route('tickets.destroy',$ticketId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

}
