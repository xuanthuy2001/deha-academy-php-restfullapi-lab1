<?php

namespace Tests\Feature\Tickets;

use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use App\Models\Ticket;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTicketTest extends TestCase
{
    /** @test */
    public  function  user_can_create_ticket_if_data_is_valid()
    {
        $dataCreate = [
            'name'=>"test Name",
            'price'=>432477744,
        ];

        $response = $this->json('POST',route('tickets.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("data")->etc();
        }
        );

        $this->assertDatabaseHas('tickets',[
            'name'=>$dataCreate['name'],
            'price'=>$dataCreate['price']
        ]);
    }
    /** @test */
    public  function  user_can_not_create_ticket_if_name_is_null()
    {
        $dataCreate = [
            'name'=>'',
            'price'=>432423534,
        ];

        $response = $this->postJson(route('tickets.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("errors", function (AssertableJson $json) {
                return $json->has('name')->etc();
            }
            )->etc();
        }
        );
    }
    /** @test */
    public  function  user_can_not_create_ticket_if_price_is_null()
    {
        $dataCreate = [
            'name'=>'test',
            'price'=>null,
        ];

        $response = $this->postJson(route('tickets.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("errors", function (AssertableJson $json) {
                return $json->has('price')->etc();
            }
            )->etc();
        }
        );
    }
    /** @test */
    public  function  user_can_not_create_ticket_if_name_and_price_is_null()
    {
        $dataCreate = [
            'name'=>'',
            'price'=>null,
        ];

        $response = $this->postJson(route('tickets.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("errors", function (AssertableJson $json) {
                return $json->has('price')->has('name')->etc();
            }
            )->etc();
        }
        );
    }
}
