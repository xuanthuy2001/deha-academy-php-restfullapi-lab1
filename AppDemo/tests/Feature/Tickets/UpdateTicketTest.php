<?php

namespace Tests\Feature\Tickets;

use App\Models\Ticket;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTicketTest extends TestCase
{

    /** @test */
    public  function  user_can_update_ticket_if_ticket_exits_and_data_is_valid()
    {
        $ticket = Ticket::factory()->create();
        $dataUpdate = [
            'name'=>"test Name",
            'price'=>999999,
        ];

        $response = $this->json('PUT',route('tickets.update',$ticket->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("data")->etc();
        }
        );

        $response->assertJson(function (AssertableJson $json) use ($dataUpdate)  {
            return $json->has("data", function (AssertableJson $json) use ($dataUpdate) {
                return $json->where('name', $dataUpdate['name'])
                        ->etc();
            }
            )->etc();
        }
        );

        $this->assertDatabaseHas('tickets',[
            'name'=>$dataUpdate['name'],
            'price'=>$dataUpdate['price']
        ]);
    }

    /** @test */
    public  function  user_can_not_update_ticket_if_ticket_exits_and_name_is_null(){
        $ticket = Ticket::factory()->create();
        $dataUpdate = [
            'name'=>'',
            'price'=>999999,
        ];

        $response = $this->json('PUT',route('tickets.update',$ticket->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("errors", fn(AssertableJson $json)=>
                $json->has('name')
            )->etc();
        }
        );


    }


    /** @test */
    public  function  user_can_not_update_ticket_if_ticket_exits_and_price_is_null(){
        $ticket = Ticket::factory()->create();
        $dataUpdate = [
            'name'=>'test name',
            'price'=>null,
        ];

        $response = $this->json('PUT',route('tickets.update',$ticket->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("errors", fn(AssertableJson $json)=>
            $json->has('price')
            )->etc();
        }
        );


    }

    /** @test */
    public  function  user_can_not_update_ticket_if_ticket_exits_and_price_is_null_and_name_is_null(){
        $ticket = Ticket::factory()->create();
        $dataUpdate = [
            'name'=>'',
            'price'=>null,
        ];

        $response = $this->json('PUT',route('tickets.update',$ticket->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(function (AssertableJson $json) {
            return $json->has("errors", fn(AssertableJson $json)=>
            $json->has('price')->has('name')->etc()
            )->etc();
        }
        );
    }

    /** @test */
    public  function  user_can_not_update_ticket_if_ticket_not_exits_and_data_is_valid(){
        $ticketId= -1;
        $dataUpdate = [
            'name'=>"test name",
            'price'=>8888
        ];


        $response = $this->json('PUT',route('tickets.update',$ticketId),$dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
